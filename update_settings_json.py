import argparse
import json
import sys
from pathlib import Path


def argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--presets", "-p", default=None)
    parser.add_argument("--font", "-f", default=None)
    parser.add_argument("--fontSize", "-s", type=int, default=0)
    parser.add_argument("--editor-fontSize", "-e", type=int, default=0)
    parser.add_argument("--editor-tabSize", "-t", type=int, default=0)
    parser.add_argument("--editor-hover-delay", "-d", type=int, default=0)
    parser.add_argument("--macos", "-m", action="store_true")
    parser.add_argument("--gitignore", "-g", action="store_true")
    return parser


fontSizes = [
    "editor.fontSize",
    "debug.console.fontSize",
    "terminal.integrated.fontSize",
]
args = argparser().parse_args()
print(args)
vscode = Path(".vscode")
cname = vscode / "settings.json"
conf = {}
if cname.is_file():
    with open(cname, encoding="utf-8") as f:
        conf = json.load(f)
if args.presets is not None:
    fnames = args.presets.split(",")
    for fname in fnames:
        with open(fname) as f:
            conf.update(json.load(f))
if args.font is not None:
    conf.update(
        {
            "editor.fontFamily": args.font,
            "debug.console.fontFamily": args.font,
            "editor.fontSize": 14,
            "editor.lineHeight": 1.3,
            "debug.console.fontSize": 14,
            "terminal.integrated.fontSize": 14,
        }
    )
if args.macos:
    conf["terminal.integrated.lineHeight"] = 1.3
else:
    if "terminal.integrated.lineHeight" in conf:
        del conf["terminal.integrated.lineHeight"]
if args.fontSize > 0:
    for k in fontSizes:
        conf[k] = args.fontSize
elif args.fontSize == 0:
    for k in fontSizes:
        try:
            del conf[k]
        except:
            pass
if args.editor_fontSize > 0:
    conf["editor.fontSize"] = args.editor_fontSize
elif args.editor_fontSize == 0:
    if "terminal.integrated.fontSize" in conf:
        conf["editor.fontSize"] = conf["terminal.integrated.fontSize"]
    else:
        try:
            del conf["editor.fontSize"]
        except:
            pass
if args.editor_tabSize > 0:
    conf["editor.tabSize"] = args.editor_tabSize
elif args.editor_tabSize == 0:
    try:
        del conf["editor.tabSize"]
    except:
        pass
if args.editor_hover_delay > 0:
    conf["editor.hover.delay"] = args.editor_hover_delay
print(json.dumps(conf, indent=2))
cname.parent.mkdir(exist_ok=True, parents=True)
with open(cname, "w", encoding="utf-8") as f:
    json.dump(conf, indent=2, fp=f)
if args.gitignore:
    with open(vscode / ".gitignore", "w", encoding="utf-8") as f:
        f.writelines(["*.log\n", "settings.json\n"])

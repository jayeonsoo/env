for x in $@; do
  if [ -d "$dir/$x" ]; then
    cd "$dir/$x"
    git pull
  else
    mkdir -p $dir
    cd $dir
    git clone ssh://primecodes/primecodes/$x
  fi
done
echo sync alphasquare...
if [ -d "/workspace/stock/alphasquare" ]; then
  cd "/workspace/stock/alphasquare"
  git pull
else
  mkdir -p /workspace/stock
  cd /workspace/stock
  git clone ssh://m8228308850/m8228308850/alphasquare
fi
for x in $@; do
  sh /workspace/env/sync-alphasquare-downloader.sh $x
done
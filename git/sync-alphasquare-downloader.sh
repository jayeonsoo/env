echo sync alphasquare_downloader_$1...
if [ -d "/workspace/stock/alphasquare_downloader_$1" ]; then
  cd "/workspace/stock/alphasquare_downloader_$1"
  git pull
else
  mkdir -p /workspace/stock
  cd /workspace/stock
  git clone ssh://m8228308850/m8228308850/alphasquare_downloader_$1
fi
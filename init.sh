cp /workspace/env/gitconfig ~/.gitconfig
cat ~/.gitconfig
mkdir -p ~/.ssh
cp /workspace/env/ssh/config ~/.ssh/config
rm -f ~/ssh-keys
ln -s /workspace/ssh-keys ~
cp /workspace/env/zsh/rc ~/.zshrc
